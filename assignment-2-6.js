document.write("<p> 6. You are given an email as string myEmail, make sure it is in correct email format. \
Should be 1 or more characters, then @ sign, then 1 or more characters, then dot, then one or more characters<br> \
 - no whitespace<br> \
 i.e. foo@bar.baz<br> \
Hints:<br> \
Use rubular to check a few emails: https://rubular.com/<br> \
Use regexp test method https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/RegExp/test \
</p><br><br>") ;



function validateEmail(email) {
    const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
}

var MattsEmail = validateEmail("savelono@uw.edu") ;

document.write("<p>Thank you stackoverflow!</p><br>") ;

document.write(`Matt Birklands email (savelono@wa.edu) Valid = ${MattsEmail}`) ;