From python:latest

WORKDIR /app

COPY requirements.txt /app/

RUN pip install --no-cache-dir -r requirements.txt

COPY src /app/

EXPOSE 8000

CMD uvicorn main:app --port 8000 --host 0.0.0.0