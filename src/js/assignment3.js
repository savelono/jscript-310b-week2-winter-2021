document.write("<b>3. Create an array to represent this tic-tac-toe board,</b><br>")
document.write("-O-<br>")
document.write("-XO<br>")
document.write("X-X<br>")

var ticTactoe = [["_","O","_"],["_","X","O"],["X","_","X"]]

document.write("<br><br><b>JavaScript array of three arrays (each line on the tictactoe board): </b>")
document.write(`<p>${ticTactoe[0]}<br>${ticTactoe[1]}<br>${ticTactoe[2]}</p>`)