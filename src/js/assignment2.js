// 2. console.log best friend's firstName and your favorite food

matt = {
    firstName: "Matt",
    lastName: "Birkland",
    age: 41,
    favFood: "Spuds Fish and Chips",
    bestFriend: "${virginia.firstName} ${virginia.lastName}"
} ;

virginia = {
    firstName: "Virginia",
    lastName: "Lau",
    age: 38,
    favFood: "fettuccine alfredo"
} ;


document.write(`<b>${matt.firstName}'s favorite food is ${matt.favFood}!</b><br></br>`) ;
document.write(`<b>${matt.firstName}'s best friend is ${virginia.firstName} ${virginia.lastName}!</b>`) ;

// Matt's Favorite Food
console.log(`Favorite Food: ${matt.favFood}`) ;

// Matt's Best Friend
console.log(`Best Friend: ${virginia.firstName} ${virginia.lastName}`) ;
