from fastapi import FastAPI, Request
from fastapi.responses import HTMLResponse
from fastapi.staticfiles import StaticFiles
from fastapi.templating import Jinja2Templates

app = FastAPI()
app.mount("/static", StaticFiles(directory="static"), name="static")
app.mount("/js", StaticFiles(directory="js"), name="js")
templates = Jinja2Templates(directory="templates")

@app.get("/", response_class=HTMLResponse)
async def web_root_template(request: Request):
    """
    Main page displays template to run JS

    :return: response
    """
    return templates.TemplateResponse("index.html", {"request": request})


# @app.get("/assignment/3", response_class=HTMLResponse)
# async def web_root_template(request: Request):
#     """
#     displays template containing JavaScript
#     assignment

#     :return: response
#     """
    
#     html_stuff = f"""
#     <!DOCTYPE html>
#     <html>
#     <head>
#     <title>JS 310b Assignment 3</title>
#     <link href="/static/styles.css" rel="stylesheet">
#     <meta charset "utf-8">
#     <h1>JS 310b Assignment 3</h1>
#     </head>
#     <body>
#     <script src="/js/assignment3.js"></script>
#     </body>
#     </html>
#     """
#     return HTMLResponse(content=html_stuff, status_code=200)

@app.get("/assignment/{assignment_num}", response_class=HTMLResponse)
def web_root_template(request: Request,assignment_num:int):
    """
    displays template containing JavaScript
    assignment

    :return: response
    """

    return templates.TemplateResponse("assignment.html", {"request": request,"assignment": assignment_num})