document.write("<p>8. Create a new Date instance to represent the dueDate.<br>  \
This will be exactly 7 days after the assignment date.</p><br<br>") ;

function addDays(date, days) {
    var result = new Date(date);
    result.setDate(result.getDate() + days);
    return result;
  }


const assignmentDate = '1/21/2019' ;
var convertStringDate = new Date(2019,21,1) ;
var newDueDate = addDays(convertStringDate,7) ;


document.write(`<p>Orginal date: ${convertStringDate}</p>`) ;
document.write(`<p>New due date: ${newDueDate}</p>`) ;

