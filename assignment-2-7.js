document.write("<p>7. You are given an assignmentDate as a string in the format \"month/day/year\"<br> \
i.e. '1/21/2019' - but this could be any date.<br> \
Convert this string to a Date<br> \
const assignmentDate = '1/21/2019';</p><br><br>") ;

const assignmentDate = '1/21/2019' ;

convertStringDate = new Date(2019,21,1) ;

document.write(`<p>The converted string using date() is ${convertStringDate}. \
I hope this is what you were asking for?  But maybe this problem was to parse the \
string with a RE and then use those values as aguments...  Let me know I'll fix it!</p>`) ;
